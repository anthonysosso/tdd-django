from django.conf.urls import patterns, include, url
from django.contrib import admin

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'polls.views.home'),
    # Examples:
    # url(r'^$', 'mysite2.views.home', name='home'),
    # url(r'^mysite2/', include('mysite2.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^poll/(\d+)/$', 'polls.views.poll'),
     url(r'^admin/', include(admin.site.urls)),
)
